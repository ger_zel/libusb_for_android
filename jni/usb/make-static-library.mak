# This variable contains the path of bin directory which contains all the gnu tools required for compilation
# I have extracted the standalone toolchain for Android platform 14 in e:\android\standalone-14"
ANDROID_NDK_BIN:=/media/Home/soft/standalone-16/bin
# Compilation Tools
CC := $(ANDROID_NDK_BIN)/arm-linux-androideabi-gcc
CPP := $(ANDROID_NDK_BIN)/arm-linux-androideabi-g++
AR := $(ANDROID_NDK_BIN)/arm-linux-androideabi-ar
# Compiler flags
# Specify all the flags below which you want to use for your compilation, For this simple example, we only need to specify the include directory path
DEBUG    = 
INCLUDES = -I./ -I./os
CFLAGS   = $(DEBUG) -Wall $(INCLUDES) -c

OBJDIR   = obj

SRCS    := $(shell find . -name '*.c')
SRCDIRS := $(shell find . -name '*.c' -exec dirname {} \; | uniq)
OBJS    := $(patsubst %.c,$(OBJDIR)/%.o,$(SRCS))

APP = libusb.a

all: buildrepo $(APP)

$(APP): $(OBJS)
	$(AR) -crs $(APP) $(OBJS)

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

buildrepo:
	@$(call make-repo)

define make-repo
	for dir in $(SRCDIRS); \
	do \
	mkdir -p $(OBJDIR)/$$dir; \
	done
endef
